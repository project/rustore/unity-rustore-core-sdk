package ru.rustore.unitysdk.core

import ru.rustore.sdk.core.util.RuStoreUtils

object RuStoreUnityCoreClient {

    fun isRuStoreInstalled(): Boolean =
        PlayerProvider.getCurrentActivity().let {
            RuStoreUtils.isRuStoreInstalled(it)
        }

    fun openRuStoreDownloadInstruction() =
        PlayerProvider.getCurrentActivity().let {
            RuStoreUtils.openRuStoreDownloadInstruction(it)
        }

    fun openRuStore() =
        PlayerProvider.getCurrentActivity().let {
            RuStoreUtils.openRuStore(it)
        }

    fun openRuStoreAuthorization() =
        PlayerProvider.getCurrentActivity().let {
            RuStoreUtils.openRuStoreAuthorization(it)
        }
}
