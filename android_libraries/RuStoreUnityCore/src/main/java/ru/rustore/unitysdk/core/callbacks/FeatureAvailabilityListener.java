package ru.rustore.unitysdk.core.callbacks;

import ru.rustore.sdk.core.feature.model.FeatureAvailabilityResult;

public interface FeatureAvailabilityListener {

    void OnFailure(Throwable throwable);
    void OnSuccess(FeatureAvailabilityResult response);
}
