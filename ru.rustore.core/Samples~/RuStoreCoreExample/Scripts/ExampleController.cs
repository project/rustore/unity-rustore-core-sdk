using RuStore.CoreClient;
using UnityEngine;

namespace RuStore.CoreExample {

    public class ExampleController : MonoBehaviour {

        public void IsRuStoreInstalled() {
            bool result = RuStoreCoreClient.Instance.IsRuStoreInstalled();
            ShowToast(string.Format("Is RuStore Installed: {0}", result));
        }

        public void openRuStoreDownloadInstruction() {
            RuStoreCoreClient.Instance.openRuStoreDownloadInstruction();
        }

        public void openRuStore() {
            RuStoreCoreClient.Instance.openRuStore();
        }

        public void openRuStoreAuthorization() {
            RuStoreCoreClient.Instance.openRuStoreAuthorization();
        }

        public void ShowToast(string message) {
            using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            using (AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            using (AndroidJavaObject utils = new AndroidJavaObject("com.plugins.coreexample.RuStoreCoreAndroidUtils")) {
                utils.Call("showToast", currentActivity, message);
            }
        }
    }
}
